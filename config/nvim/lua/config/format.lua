vim.api.nvim_create_augroup("AutoFormat", {})

vim.api.nvim_create_autocmd("BufWritePost", {
	pattern = "*.lua",
	group = "AutoFormat",
	callback = function()
		vim.cmd("silent !stylua %")
		vim.cmd("edit")
	end,
})
