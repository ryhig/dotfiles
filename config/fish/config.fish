if status is-interactive
    # Commands to run in interactive sessions can go here
end

fish_add_path ~/.config/emacs/bin

fish_add_path ~/go/bin
