function ls --wraps='exa -la --sort oldest' --wraps='exa -lar --sort oldest' --description 'alias ls=exa -lar --sort oldest'
  exa -lar --sort oldest $argv
        
end
